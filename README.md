# Story 9 | Authentication, Cookie, and Session
Authored by:

Naufal Thirafy Prakoso

1806191723

PPW-D

# Pipeline and Coverage
[![pipeline status](https://gitlab.com/naufal.prakoso/story-9/badges/master/pipeline.svg)](https://gitlab.com/naufal.prakoso/story-9/commits/master)

[![coverage report](https://gitlab.com/naufal.prakoso/story-9/badges/master/coverage.svg)](https://gitlab.com/naufal.prakoso/story-9/commits/master)

# Herokuapp Link
copalogin.herokuapp.com